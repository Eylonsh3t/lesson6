#pragma once

class mathUtils
{
public:
	static double CalPentagonArea(double edge);
	static double CalHexagonArea(double edge);
};