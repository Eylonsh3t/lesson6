#pragma once
#include "shape.h"
#include "mathUtils.h"

class hexagon : public Shape, public mathUtils
{
	double _edge;
public:

	hexagon(std::string nam, std::string col, double edge);
	void draw();
	void setEdge(double edge);
};