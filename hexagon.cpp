#include <iostream>
#include "hexagon.h"

hexagon::hexagon(std::string nam, std::string col, double edge) : Shape(col, nam)
{
	this->_edge = edge;
}

void hexagon::draw()
{
	std::cout << "The edge: " << this->_edge << std::endl;
	std::cout << "Area: " << this->CalHexagonArea(this->_edge) << std::endl;
}

void hexagon::setEdge(double edge)
{
	this->_edge = edge;
}