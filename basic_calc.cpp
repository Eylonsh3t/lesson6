#include <iostream>
#define ERROR_NUM 8200
#define ERROR_RETURN -1


int add(int a, int b, int& check_if_error)
{
	if (a == ERROR_NUM || b == ERROR_NUM || a + b == ERROR_NUM)
	{
		std::cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.\n";
		check_if_error = 1;
		return ERROR_RETURN;
	}
	return a + b;
}

int multiply(int a, int b, int& check_if_error)
{
	int* check = &check_if_error;
	if (b == ERROR_NUM)
	{
		std::cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.\n";
		check_if_error = 1;
		return ERROR_RETURN;
	}
	int sum = 0;
	for(int i = 0; i < b; i++)
	{
		if (*check == 1)
		{
			check_if_error = 1;
			break;
		}
		sum = add(sum, a, *check);
	};
	return sum;
}

int pow(int a, int b, int& check_if_error)
{
	int* check = &check_if_error;
	if (b == ERROR_NUM)
	{
		std::cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.\n";
		check_if_error == 1;
		return ERROR_RETURN;
	}
	int exponent = 1;
	for(int i = 0; i < b; i++)
	{
		if (*check == 1)
		{
			check_if_error = 1;
			break;
		}
		exponent = multiply(exponent, a, *check);
	};
	return exponent;
}

int main(void)
{
	int result;
	int* check_if_error = new int[1];
	result = add(5, 0, *check_if_error);
	if (*check_if_error != 1)
	{
		std::cout << result << std::endl;
	}
	*check_if_error = 0;
	result = multiply(10, 2, *check_if_error);
	if (*check_if_error != 1)
	{
		std::cout << result << std::endl;
	}
	*check_if_error = 0;
	result = pow(8200, 2, *check_if_error);
	if (*check_if_error != 1)
	{
		std::cout << result << std::endl;
	}

	delete[] check_if_error;
	return 0;
}