#include <iostream>
#include "pentagon.h"

pentagon::pentagon(std::string nam, std::string col, double edge) : Shape(col, nam)
{
	this->_edge = edge;
}

void pentagon::draw()
{
	std::cout << "The edge: " << this->_edge << std::endl;
	std::cout << "Area: " << this->CalPentagonArea(this->_edge) << std::endl;
}

void pentagon::setEdge(double edge)
{
	this->_edge = edge;
}