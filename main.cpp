#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "pentagon.h"
#include "hexagon.h"

#define MAX_SIZE 100
#define CHAR_SIZE 1
#define MAX_ANG 180
#define MIN_ANG 0

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0, edge = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	pentagon pent(nam, col, edge);
	hexagon hex(nam, col, edge);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrpent = &pent;
	Shape* ptrhex = &hex;


	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p', pentagon = 'e', hexagon = 'h'; char* shapetype = new char[MAX_SIZE];
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, e = pentagon, h = hexagon." << std::endl;
		std::cin >> shapetype;
		if (strlen(shapetype) > CHAR_SIZE)
		{
			shapetype[0] = 1;
		}
try
		{

			switch (shapetype[0]) {
			case 'c':
				std::cout << "enter color for circle" << std::endl;
				std::cin >> col;
				circ.setColor(col);
				std::cout << "enter name for circle" << std::endl;
				std::cin >> nam;
				circ.setName(nam);
				std::cout << "enter radius for circle" << std::endl;
				std::cin >> rad;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(256, '\n');
					throw inputException();
				}
				if (rad < 0)
				{
					throw shapeException();
				}
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name" << std::endl;
				std::cin >> nam;
				quad.setName(nam);
				std::cout << "enter color" << std::endl;
				std::cin >> col;
				quad.setColor(col);
				std::cout << "enter height" << std::endl;
				std::cin >> height;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(256, '\n');
					throw inputException();
				}
				if (height < 0)
				{
					throw shapeException();
				}
				quad.setHeight(height);
				std::cout << "enter width" << std::endl;
				std::cin >> width;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(256, '\n');
					throw inputException();
				}
				if (width < 0)
				{
					throw shapeException();
				}
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name" << std::endl;
				std::cin >> nam;
				rec.setName(nam);
				std::cout << "enter color" << std::endl;
				std::cin >> col;
				rec.setColor(col);
				std::cout << "enter height" << std::endl;
				std::cin >> height;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(256, '\n');
					throw inputException();
				}
				if (height < 0)
				{
					throw shapeException();
				}
				rec.setHeight(height);
				std::cout << "enter width" << std::endl;
				std::cin >> width;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(256, '\n');
					throw inputException();
				}
				if (width < 0)
				{
					throw shapeException();
				}
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name" << std::endl;
				std::cin >> nam;
				para.setName(nam);
				std::cout << "enter color" << std::endl;
				std::cin >> col;
				para.setColor(col);
				std::cout << "enter height" << std::endl;
				std::cin >> height;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(256, '\n');
					throw inputException();
				}
				if (height < 0)
				{
					throw shapeException();
				}
				para.setHeight(height);
				std::cout << "enter width" << std::endl;
				std::cin >> width;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(256, '\n');
					throw inputException();
				}
				if (width < 0)
				{
					throw shapeException();
				}
				para.setWidth(width);
				std::cout << "enter angle" << std::endl;
				std::cin >> ang;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(256, '\n');
					throw inputException();
				}
				if (ang < MIN_ANG || ang > MAX_ANG)
				{
					throw shapeException();
				}
				para.setAngle1(ang);
				std::cout << "enter another angle" << std::endl;
				std::cin >> ang2;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(256, '\n');
					throw inputException();
				}
				if (ang2 < MIN_ANG || ang2 > MAX_ANG)
				{
					throw shapeException();
				}
				para.setAngle2(ang2);
				ptrpara->draw();

			case 'e':
				std::cout << "enter name" << std::endl;
				std::cin >> nam;
				pent.setName(nam);
				std::cout << "enter color" << std::endl;
				std::cin >> col;
				pent.setColor(col);
				std::cout << "enter edge" << std::endl;
				std::cin >> edge;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(256, '\n');
					throw inputException();
				}
				if (edge < 0)
				{
					throw shapeException();
				}
				pent.setEdge(edge);
				ptrpent->draw();
				break;
			case 'h':
				std::cout << "enter name" << std::endl;
				std::cin >> nam;
				hex.setName(nam);
				std::cout << "enter color" << std::endl;
				std::cin >> col;
				hex.setColor(col);
				std::cout << "enter edge" << std::endl;
				std::cin >> edge;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(256, '\n');
					throw inputException();
				}
				if (edge < 0)
				{
					throw shapeException();
				}
				hex.setEdge(edge);
				ptrhex->draw();
				break;
			case 1:
				std::cout << "You have chosen 2 letters instead of 1." << std::endl;
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin >> x;
		}
		catch (std::exception& e)
		{			
			std::cout << (e.what());
		}
		catch (...)
		{
			std::cout << "caught a bad exception. continuing as usual\n";
		}
	}


	delete[] shapetype;
	system("pause");
	return 0;
	
}