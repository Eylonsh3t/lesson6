#include <iostream>
#define ERROR_NUM 8200
#define ERROR_RETURN -1

int add(int a, int b)
{
	if (a == ERROR_NUM || b == ERROR_NUM || a + b == ERROR_NUM)
	{
		throw(ERROR_RETURN);
	}
	return a + b;
}

int multiply(int a, int b)
{
	if (b == ERROR_NUM)
	{
		throw(ERROR_RETURN);
	}
	int sum = 0;
	for (int i = 0; i < b; i++)
	{
		sum = add(sum, a);
	};
	return sum;
}

int pow(int a, int b)
{
	if (b == ERROR_NUM)
	{
		throw(ERROR_RETURN);
	}
	int exponent = 1;
	for (int i = 0; i < b; i++)
	{
		exponent = multiply(exponent, a);
	};
	return exponent;
}

int main(void)
{
	try
	{
		int result = add(8200, 0);
		std::cout << result << std::endl;
	}
	catch(...)
	{
		std::cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.\n";
	}
	try
	{

		int result = multiply(200, 2);
		std::cout << result << std::endl;
	}
	catch(...)
	{
		std::cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.\n";
	}
	try
	{

		int result = pow(200, 2);
		std::cout << result << std::endl;
	}
	catch (...)
	{
		std::cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.";
	}

	return 0;
}