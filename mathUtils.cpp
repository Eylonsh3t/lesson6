#include "mathUtils.h"
#define PART_OF_CALC_PENT 1.7
#define PART_OF_CALC_HEX 2.6

double mathUtils::CalPentagonArea(double edge)
{
	return double(PART_OF_CALC_PENT * (edge * edge));
}

double mathUtils::CalHexagonArea(double edge)
{
	return double(PART_OF_CALC_HEX * (edge * edge));
}
