#pragma once
#include "shape.h"
#include "mathUtils.h"

class pentagon : public Shape , public mathUtils
{
	double _edge;
public:

	pentagon(std::string nam, std::string col, double edge);
	void draw();
	void setEdge(double edge);
};